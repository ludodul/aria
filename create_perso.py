from jinja2 import Template

with open('vierge_name.svg','r') as f:
    r = f.read()

template = Template(r)

perso = {
    'nom': 'Merlin'
}

out = template.render(**perso)

with open('output.svg','w') as o:
    o.write(out)